# README

- Introduction
  - [Overview](#Overview)
  - [Pre-requisites](#pre-requisites)
  - [Deploy](#deploy)
  - [Test](#test)
  - [Troubleshooting](#troubleshooting)

# Overview
This is a quick proof of ability to show that I am able to design a docker-compose stack with a monitored Nginx container 

This setup is not secure and not optimized, do not run these containers in production  

this stack is divided in 2 networks: 
- front-tier > facing public
- back-tier > facing private

the services in this stack are:
- nginx:1.21.4 - Monitored webserver
- nginx-exporter:0.9.0 - service responsible for parsing the webserver metrics in a readble way for prometheus
- prometheus:v2.31.1 - service responsible for monitoring other services and send alerts to the alert manager
- alertmanager:v0.23.0 - service responsible for sending alerts to systems / humans in case an event needs to send alerts

# Pre-requisites
- A linux machine
- git installed on the machine
- Docker engine installed on the machine
- docker-compose installed on the machine
- the user nobody on the machine with id 65534
- Free ports on the machine:
    + 80 used by nginx
    + 8080 used by nginx to show metrics
    + 9090 used by Prometheus
    + 9093 used by Alertmanager
    + 9113 used by nginx-exporter

# Deploy

Clone the project locally to your host and move in the project folder

Run the containers as daemons

        docker-compose up -d

Review the containers

        docker ps 
    
You should be able to see all containers running 

~~~
CONTAINER ID        IMAGE                                   COMMAND                  CREATED             STATUS              PORTS                                          NAMES
e30bdb8761e1        nginx/nginx-prometheus-exporter:0.9.0   "/usr/bin/nginx-prom…"   6 seconds ago       Up 4 seconds        0.0.0.0:9113->9113/tcp                         testing_nginx-exporter_1
299fbc2362ab        nginx:1.21.4                            "/docker-entrypoint.…"   6 seconds ago       Up 5 seconds        0.0.0.0:80->80/tcp, 127.0.0.1:8080->8080/tcp   testing_nginx_1
dbf7424487c7        prom/prometheus:v2.31.1                 "/bin/prometheus --c…"   7 seconds ago       Up 5 seconds        0.0.0.0:9090->9090/tcp                         testing_prometheus_1
f36e116f5c53        prom/alertmanager:v0.23.0               "/bin/alertmanager -…"   7 seconds ago       Up 6 seconds        0.0.0.0:9093->9093/tcp                         testing_alertmanager_1
~~~


- Nginx should deliver the index page at http://localhost

- nginx-exporter should deliver metrics at http://localhost:9113/metrics

- Prometheus should show the UI at http://localhost:9090

- Alertmanager should show the UI at http://localhost:9093


# Test

- Stop the webserver to test the alerts


        docker-compose stop nginx


- An error should be shown at http://localhost

- After some seconds  ( more than 10 ) Alertmanager should show an InstanceDown alerts in the UI at http://localhost:9093/#/alerts  

# Reset

Stop all containers and clean setup

        docker-compose down --volumes

# Troubleshooting

Prometheus and Alertmanager might need the configurations to be owned by the nobody user

        chown -R nobody:nobody ./etc/alertmanager
        chown -R nobody:nobody ./etc/prometheus